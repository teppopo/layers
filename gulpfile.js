var gulp = require('gulp');

var concat = require('gulp-concat');
var clean = require('gulp-clean-css');
var uglify = require('gulp-uglify');

gulp.task('css', function(){
    return gulp
        .src(['css/layers.css'])
        .pipe(concat('layers.min.css'))
        .pipe(clean({processImport: false}))
        .pipe(gulp.dest('css'));
});
gulp.task('content', function(){
    return gulp
        .src(['css/layers-content.css'])
        .pipe(concat('layers-content.min.css'))
        .pipe(clean({processImport: false}))
        .pipe(gulp.dest('css'));
});
gulp.task('js', function(){
    return gulp
        .src(['js/layers.js'])
        .pipe(concat('layers.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('default', ['css', 'content', 'js'], function(){
    gulp.watch("css/layers.css", ['css']);
    gulp.watch("css/layers-content.css", ['content']);
    gulp.watch("js/*.js", ['js']);
});