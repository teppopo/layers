var body = document.body;
var layers = document.getElementsByClassName('layer');

//load images to layers
body.style.backgroundImage = 'url('+layers[0].getAttribute('img')+')';
body.style.backgroundColor = layers[0].getAttribute('color');
for(var i=1;i<layers.length;i++){
	var img = document.createElement('img');
	img.src = layers[i].getAttribute('img')||'undefined';
	img.className = 'layer-header';
	img.i = i;
	img.onerror = function(){
		layers[this.i].removeChild(this);
		var div = document.createElement('div');
		div.style.position = 'absolute';
		div.style.zIndex = '-1';
		div.style.width = '100%';
		div.style.height = '100%';
		div.style.marginTop = '-35%';
		div.style.backgroundColor = layers[this.i].getAttribute('color');
		layers[this.i].appendChild(div);
	}
	layers[i].appendChild(img);
	layers[i].style.backgroundColor = layers[i].getAttribute('color');
}

//menu button
var count = 0;
function initList(){
	var list = menuList.getElementsByTagName("li");
	for(var i=0;i<list.length;i++){
		list[i].i = i;
		list[i].onclick = function(){
			if(menuButton.clicked){
				var i = this.i;
				var down = ((i*window.innerHeight)-(document.documentElement.scrollTop||body.scrollTop)>0 ? true:false);
				var height = (down ? ((i*window.innerHeight)-(document.documentElement.scrollTop||body.scrollTop))/*/100*/:((i*window.innerHeight)-(document.documentElement.scrollTop||body.scrollTop))/*/100*/);
				height = (body.offsetWidth>600? height-240:height-480);
				var test = setInterval(function(){
					if(count<15){
						height = height*0.5;
						document.documentElement.scrollTop += height;
						body.scrollTop += height;
						count++;
					}else{
						count = 0;
						clearInterval(test);
					}
				},50);
			}
		}
	}
}

var menuBack = document.createElement('div');
menuBack.id = 'menu-back';
body.appendChild(menuBack);
var menuGroup = document.getElementById('menu-group');
var menuButton = document.getElementById('menu-button');
var menuList = document.getElementById('menu-list');
initList();

menuButton.clicked = false;
menuButton.onclick = function(){
	menuButton.clicked = !menuButton.clicked;
	var marginTop = '';
	var list = menuList.getElementsByTagName("li");
	
	if(menuButton.clicked){
		menuBack.style.background = 'rgba(0,0,0,0.7)';
		menuBack.style.width = '30%';
		menuButton.style.opacity = '0.8';
		menuList.style.opacity = '1';
		menuList.style.visibility = 'visible';
		menuList.style.pointerEvents = 'none';
		marginTop = (body.offsetWidth>600 ? '-25px':'-5px');
	}else{
		menuBack.style.background = 'rgba(0,0,0,0)';
		menuBack.style.width = '0%';
		menuButton.style.opacity = '0.4';
		menuList.style.opacity = '0';
		menuList.style.visibility = 'hidden';
		menuList.style.pointerEvents = 'visible';
		marginTop = (body.offsetWidth>600 ? '-70px':'-30px');
	}
	
	for(var i=0;i<list.length;i++){
		list[i].style.marginTop = marginTop;
		list[i].style.transition = '0.5s';
		list[i].style.pointerEvents = (menuButton.clicked? 'visible':'none');
	}
}
var dragStartPoint = undefined;
menuList.onmousedown = function(e){
	dragStartPoint = e.pageY;
}
menuList.onmousemove = function(e){
	if(dragStartPoint){
		if(e.pageY>dragStartPoint){
			menuList.scrollTop -= 10;
		}else{
			menuList.scrollTop += 10;
		}
	}
}
menuList.onmouseup = function(){
	dragStartPoint = undefined;
}
//mobile
var prevScroll = 0;
window.onscroll = function(){
	if(menuButton.clicked){
		var scrollChecker = body.scrollTop||document.documentElement.scrollTop;
		if(scrollChecker>prevScroll){
			menuList.scrollTop += 10;
		}else{
			menuList.scrollTop -= 10;
		}
		prevScroll = scrollChecker;
	}
}

//search box
var searchGroup = document.getElementById('search-group');
var searchArea = document.getElementById('search-area');
var searchButton = document.getElementById('search-button');
var searchBox = document.getElementById('search-box');
if(searchGroup &&searchArea &&searchButton &&searchBox){
	searchArea.onclick = function(){
		searchGroup.style.visibility = 'hidden';
		searchArea.style.opacity = '0';
		searchBox.style.opacity = '0';
	}
	searchButton.onclick = function(){
		searchGroup.style.visibility = 'visible';
		searchArea.style.opacity = '1';
		searchBox.style.opacity = '1';
	}
}

//logo
var logo = document.getElementById('logo');
logo.onmousedown = function(){
	var height = -(document.documentElement.scrollTop||body.scrollTop);
	height = (body.offsetWidth>600? height-240:height-480);
	var test = setInterval(function(){
		if(count<15){
			height = height*0.5;
			document.documentElement.scrollTop += height;
			body.scrollTop += height;
			count++;
		}else{
			count = 0;
			clearInterval(test);
		}
	},50);
}

//math
function orderedSum(n){
	var c = 0;
	for(var i=0;i<n;i++){
		c += i;
	}
	return c;
}

function renderContent(layer,show){
	var nodes = layer.children;
	if(show){
		for(var i=0;i<nodes.length-1;i++){
			nodes[i].style.transition = '0.5s 0.5s';
			nodes[i].style.opacity = '1';
		}
	}else{
		for(var i=0;i<nodes.length-1;i++){
			nodes[i].style.opacity = '0';
		}
	}
}

function lay(){
	//page height
	body.style.minHeight = (window.innerHeight*(layers.length-1))+'px';
	body.style.maxHeight = (window.innerHeight*(layers.length-1))+'px';
	//menu
	var list = menuList.getElementsByTagName("li");
	//logo
	var scrollChecker = body.scrollTop||document.documentElement.scrollTop;
	if(body.offsetWidth>600){
		for(var i=0;i<list.length;i++){
			list[i].style.background = 'url("'+list[i].getAttribute('img')+'") 0 bottom no-repeat';
			list[i].style.padding = '20px 0 16px 52px';
			list[i].style.top = (90+(i*30))+'px';
			list[i].style.fontSize = '14px';
			list[i].style.color = '#fff';
		}
		//logo
		if(scrollChecker==0){
			logo.style.width = '40%';
			logo.style.top = '25%';
			logo.style.right = '30%';
		}else{
			logo.style.width = '8%';
			logo.style.top = '20px';
			logo.style.right = '3%';
		}
	}else{
		for(var i=0;i<list.length;i++){
			list[i].style.background = 'none';
			list[i].style.padding = '0';
			list[i].style.top = (65+(i*30))+'px';
			list[i].style.fontSize = '14px';
			list[i].style.color = '#fff';
		}
		//logo
		if(scrollChecker==0){
			logo.style.width = '60%';
			logo.style.top = '30%';
			logo.style.right = '20%';
		}else{
			logo.style.width = '20%';
			logo.style.top = '7px';
			logo.style.right = '3%';
		}
	}
	//menu hilight
	list[Math.ceil(scrollChecker/window.innerHeight)].style.fontSize = '16px';
	list[Math.ceil(scrollChecker/window.innerHeight)].style.color = '#FFE57F';
	
	//layer margin
	var margin = (window.innerHeight/2)*(window.innerWidth/window.innerHeight)+(body.offsetWidth>600? -50:100);
	
	//layering
	for(var i=1;i<layers.length;i++){
		var order = (window.innerHeight/2)*(orderedSum(i)/orderedSum(layers.length));
		
		if((document.documentElement.scrollTop||body.scrollTop)<=((window.innerHeight*0.85)*(i-1))){
			layers[i].style.position = 'fixed';
			layers[i].style.top = ((document.documentElement.scrollTop||body.scrollTop)/layers.length-1)+margin+order+'px';
			layers[i].style.transition = '1s';
			renderContent(layers[i],false);
		}else{
			layers[i].style.position = 'absolute';
			layers[i].style.top = (window.innerHeight*(i-1))+margin+'px';
			layers[i].style.transition = 'none';
			renderContent(layers[i],true);
		}
	}
}

//initialize
lay();
body.scrollTop = body.scrollHeight;
document.documentElement.scrollTop = body.scrollHeight;

window.addEventListener('scroll',lay);
window.addEventListener('resize',lay);

//loading
var wrap = document.createElement('div');
wrap.style.cssText = 'position:fixed;top:0;left:0;width:100%;height:100%;background:#fff;transition:0.5s;';
var wrap2 = document.createElement('div');
wrap2.style.cssText = 'position:absolute;left:50%;top:40%;';
var loader = document.createElement('div');
loader.className = 'loader';
wrap2.appendChild(loader);
wrap.appendChild(wrap2);
body.appendChild(wrap);

window.onload = function(){
	setTimeout(function(){
		body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
		menuGroup.style.opacity = '1';
		
		wrap.style.opacity = '0';
		setTimeout(function(){
			body.removeChild(wrap);
		},500);
	},100);
}